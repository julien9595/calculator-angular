import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'calculator-app';
    result!: number;

    onCompute(value: number) {
        console.log(value);
    }
}
