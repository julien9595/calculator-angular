import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CalculatorComponent} from './calculator.component';
import {CalculatorService} from '../../services/calculator.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {By} from '@angular/platform-browser';

class CalculatorServiceMock {
    getComputedResult() {}
}

describe('CalculatorComponent', () => {
    let component: CalculatorComponent;
    let fixture: ComponentFixture<CalculatorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                MatButtonModule
            ],
            declarations: [ CalculatorComponent ],
            providers: [
                {
                    provide: CalculatorService,
                    useClass: CalculatorServiceMock
                },
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CalculatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display correct value', () => {
        spyOn(component['_calculatorService'], 'getComputedResult').and.returnValue(11);
        component.displayedValue = '5';
        fixture.detectChanges();
        fixture.debugElement.query(By.css('#numberSeven')).nativeElement.click();
        fixture.debugElement.query(By.css('#operatorPlus')).nativeElement.click();
        fixture.debugElement.query(By.css('#numberFour')).nativeElement.click();
        fixture.debugElement.query(By.css('button.equal')).nativeElement.click();
        fixture.detectChanges();
        const displayedValue: string = fixture.debugElement.query(By.css('#displayedValue')).nativeElement.textContent;
        expect(displayedValue).toEqual('11');
    });

    describe('_containsAlreadyADot()', () => {
        it('should return false if this.displayedValue is null', () => {
            // @ts-ignore
            component.displayedValue = null;
            expect(component['_containsAlreadyADot']()).toBeFalse();
        });

        it('should return false if this.displayedValue does not include . char', () => {
            component.displayedValue = '95';
            expect(component['_containsAlreadyADot']()).toBeFalse();
        });

        it('should return true if this.displayedValue does not include . char', () => {
            component.displayedValue = '95.2';
            expect(component['_containsAlreadyADot']()).toBeTrue();
        });
    });

    describe('handleNumber()', () => {
        describe('this._operator is empty', () => {
            beforeEach(() => {
                component['_operator'] = '';
            });

            it('should call this._updateDisplayedValue() with this._firstValue and value parameter', () => {
                // @ts-ignore
                spyOn(component, '_updateDisplayedValue');
                component['_firstValue'] = '10';
                component['_secondValue'] = '100';
                component.displayedValue = '5';
                component.handleNumber('2');
                expect(component['_updateDisplayedValue']).toHaveBeenCalledWith('10', '2');
            });

            it('should update this._firstValue', () => {
                // @ts-ignore
                spyOn(component, '_updateDisplayedValue').and.callFake(() => {
                    component.displayedValue = '45'
                });
                component['_firstValue'] = '10';
                component['_secondValue'] = '100';
                component.displayedValue = '5';
                component.handleNumber('2');
                expect(component['_firstValue']).toEqual('45');
            });
        });

        describe('this._operator is not empty', () => {
            beforeEach(() => {
                component['_operator'] = '*';
            });

            it('should call this._updateDisplayedValue() with this._secondValue and value parameter', () => {
                // @ts-ignore
                spyOn(component, '_updateDisplayedValue');
                component['_firstValue'] = '100';
                component['_secondValue'] = '20';
                component.displayedValue = '5';
                component.handleNumber('2');
                expect(component['_updateDisplayedValue']).toHaveBeenCalledWith('20', '2');
            });

            it('should update this._secondValue', () => {
                // @ts-ignore
                spyOn(component, '_updateDisplayedValue').and.callFake(() => {
                    component.displayedValue = '45'
                });
                component['_firstValue'] = '100';
                component['_secondValue'] = '20';
                component.displayedValue = '5';
                component.handleNumber('2');
                expect(component['_secondValue']).toEqual('45');
            });
        });
    });

    describe('handleDot()', () => {
        it('should not call this.handleNumber() if this._containsAlreadyADot() returns true', () => {
            // @ts-ignore
            spyOn(component, '_containsAlreadyADot').and.returnValue(true);
            spyOn(component, 'handleNumber');
            component.handleDot();
            expect(component.handleNumber).not.toHaveBeenCalled();
        });

        it('should call this.handleNumber() if this._containsAlreadyADot() returns false', () => {
            // @ts-ignore
            spyOn(component, '_containsAlreadyADot').and.returnValue(false);
            spyOn(component, 'handleNumber');
            component.handleDot();
            expect(component.handleNumber).toHaveBeenCalledWith('.');
        });
    });

    describe('clear()', () => {
        it('should set default value of this._firstValue', () => {
            component['_firstValue'] = '5';
            component.clear();
            expect(component['_firstValue']).toEqual('');
        });

        it('should set default value of this._secondValue', () => {
            component['_secondValue'] = '5';
            component.clear();
            expect(component['_secondValue']).toEqual('');
        });

        it('should set default value of this._operator', () => {
            component['_operator'] = '*';
            component.clear();
            expect(component['_operator']).toEqual('');
        });

        it('should not set default value of this.displayedValue if resetDisplayedValue equals false', () => {
            component.displayedValue = '98';
            component.clear(false);
            expect(component.displayedValue).toEqual('98');
        });

        it('should set default value of this.displayedValue if resetDisplayedValue equals true', () => {
            component.displayedValue = '98';
            component.clear(true);
            expect(component.displayedValue).toEqual('');
        });
    });

});
