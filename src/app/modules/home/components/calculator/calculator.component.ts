import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CalculatorService} from '../../services/calculator.service';

@Component({
    selector: 'app-calculator',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
    private _firstValue = '';
    private _secondValue = '';
    private _operator = '';

    displayedValue = '';

    @Output() resultComputed: EventEmitter<number> = new EventEmitter<number>();

    constructor(private _calculatorService: CalculatorService) { }

    ngOnInit(): void {}

    private _containsAlreadyADot(): boolean {
        return !!this.displayedValue?.includes('.');
    }

    private _updateDisplayedValue(currentValue: string, nextValue: string): void {
        this.displayedValue = currentValue ? currentValue + nextValue : nextValue;
    }

    /**
     * If we click on number button
     * @param value
     */
    handleNumber(value: string): void {
        if (!this._operator) {
            // If no operator then we handle first value
            this._updateDisplayedValue(this._firstValue, value);
            this._firstValue = this.displayedValue;
        } else {
            // If an operator then we handle second value
            this._updateDisplayedValue(this._secondValue, value);
            this._secondValue = this.displayedValue;
        }
    }

    /**
     * If we click on operator button
     * @param operator
     */
    handleOperator(operator: string): void {
        if (!this._firstValue) {
            if (!this.displayedValue) {
                // No value has been computed before
                return;
            } else {
                // If a value has already been computed, we use it to compute next value
                this._firstValue = this.displayedValue;
            }
        }
        if (this._operator) {
            this.compute();
            this._firstValue = this.displayedValue;
        }
        this._operator = operator;
    }

    /**
     * If we click on dot button
     */
    handleDot(): void {
        if (this._containsAlreadyADot()) {
            return;
        }
        this.handleNumber('.')
    }

    /**
     * If we click on clear button
     * @param resetDisplayedValue
     */
    clear(resetDisplayedValue = true): void {
        this._firstValue = '';
        this._secondValue = '';
        this._operator = '';
        if (resetDisplayedValue) {
            this.displayedValue = '';
        }
    }

    /**
     * If we click on equal button
     */
    compute(): void {
        if (this._firstValue && this._secondValue) {
            const previousValueFloat: number = Number.parseFloat(this._firstValue);
            const currentValueFloat: number = Number.parseFloat(this._secondValue);
            const result: number = this._calculatorService.getComputedResult(previousValueFloat, currentValueFloat, this._operator);
            this.clear(false);
            this.displayedValue = result?.toString();
            this.resultComputed.emit(result);
        }
    }
}
