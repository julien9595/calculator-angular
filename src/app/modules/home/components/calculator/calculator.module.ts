import {CalculatorComponent} from './calculator.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
    declarations: [
        CalculatorComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule
    ],
    exports: [
        CalculatorComponent
    ],
})
export class CalculatorModule {}
