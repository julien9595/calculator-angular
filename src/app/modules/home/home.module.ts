import {NgModule} from '@angular/core';
import {CalculatorModule} from './components/calculator/calculator.module';

@NgModule({
    exports: [
        CalculatorModule
    ]
})
export class HomeModule {}
