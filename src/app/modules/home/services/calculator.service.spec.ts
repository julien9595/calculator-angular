import {CalculatorService} from './calculator.service';
import {TestBed, waitForAsync} from '@angular/core/testing';

describe('CalculatorService', () => {
    let service: CalculatorService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            providers: [
                CalculatorService
            ]
        });
        service = TestBed.inject(CalculatorService);
    }));

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    describe('_isDefinedAndNotNull()', () => {
        it('should return false if value parameter is null', () => {
            expect(service['_isDefinedAndNotNull'](null)).toBeFalse();
        });

        it('should return false if value parameter is undefined', () => {
            expect(service['_isDefinedAndNotNull'](undefined)).toBeFalse();
        });

        it('should return true if value parameter is defined and not null', () => {
            expect(service['_isDefinedAndNotNull'](4)).toBeTrue();
        });
    });

    describe('getComputedResult()', () => {
        it('should return 0 if this._isDefinedAndNotNull(leftValue) returns true', () => {
            const leftValue = null;
            const rightValue = 5;
            // @ts-ignore
            spyOn(service, '_isDefinedAndNotNull').withArgs(leftValue).and.returnValue(false).withArgs(rightValue).and.returnValue(true);
            // @ts-ignore
            expect(service.getComputedResult(leftValue, rightValue, '+')).toEqual(0);
        });

        it('should return 0 if this._isDefinedAndNotNull(rightValue) returns true', () => {
            const leftValue = 1;
            const rightValue = null;
            // @ts-ignore
            spyOn(service, '_isDefinedAndNotNull').withArgs(leftValue).and.returnValue(true).withArgs(rightValue).and.returnValue(false);
            // @ts-ignore
            expect(service.getComputedResult(leftValue, rightValue, '+')).toEqual(0);
        });

        it('should return 0 if no operator parameter match', () => {
            // @ts-ignore
            spyOn(service, '_isDefinedAndNotNull').and.returnValue(true);
            // @ts-ignore
            expect(service.getComputedResult(6, 2, '%')).toEqual(0);
        });

        it('should return correct computed value if operator parameter equals /', () => {
            // @ts-ignore
            spyOn(service, '_isDefinedAndNotNull').and.returnValue(true);
            // @ts-ignore
            expect(service.getComputedResult(6, 2, '/')).toEqual(3);
        });

        it('should return correct computed value if operator parameter equals *', () => {
            // @ts-ignore
            spyOn(service, '_isDefinedAndNotNull').and.returnValue(true);
            // @ts-ignore
            expect(service.getComputedResult(6, 2, '*')).toEqual(12);
        });

        it('should return correct computed value if operator parameter equals -', () => {
            // @ts-ignore
            spyOn(service, '_isDefinedAndNotNull').and.returnValue(true);
            // @ts-ignore
            expect(service.getComputedResult(6, 2, '-')).toEqual(4);
        });

        it('should return correct computed value if operator parameter equals +', () => {
            // @ts-ignore
            spyOn(service, '_isDefinedAndNotNull').and.returnValue(true);
            // @ts-ignore
            expect(service.getComputedResult(6, 2, '+')).toEqual(8);
        });
    });
});
