import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CalculatorService {
    constructor() {
    }

    /**
     * Check if value is null or undefined and returns a boolean accordingly
     * @param value
     * @private
     */
    private _isDefinedAndNotNull(value: any): boolean {
        return value !== null && typeof value !== 'undefined';
    }

    /**
     * Returns the computed value
     * @param leftValue
     * @param rightValue
     * @param operator
     */
    getComputedResult(leftValue: number,
                      rightValue: number,
                      operator: string): number {
        if (this._isDefinedAndNotNull(leftValue) &&
            this._isDefinedAndNotNull(rightValue)) {
            switch (operator) {
                case '/':
                    return leftValue / rightValue;
                case '*':
                    return leftValue * rightValue;
                case '-':
                    return leftValue - rightValue;
                case '+':
                    return leftValue + rightValue;
                default:
                    return 0;
            }
        }
        return 0;
    }
}
